const serverDefault = {
  serverStatus: {},
  keys: {},
  currentConfig: {},
  configTemplate: [],
};

export default serverDefault;
