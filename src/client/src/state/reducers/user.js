import initialState from "./init/user";
import * as actions from "state/actions";

const user = (state = initialState, action) => {
  switch (action.type) {
    case actions.USER_LOGIN_RESPONSE:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export default user;
