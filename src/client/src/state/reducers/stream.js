import initialState from "./init/stream";
import * as actions from "state/actions";

const stream = (state = initialState, action) => {
  switch (action.type) {
    case actions.STREAM_STATUS_RESPONSE:
      return { ...state, serverStatus: action.payload };
    case actions.STREAM_KEYS_RESPONSE:
      let keys = { ...state.keys };
      keys[action.payload.name] = action.payload;
      return { ...state, keys };
    case actions.CONFIG_GET_RESPONSE:
      return { ...state, currentConfig: action.payload };
    case actions.CONFIG_GET_TEMPLATE_RESPONSE:
      return { ...state, configTemplate: action.payload };
    default:
      return state;
  }
};

export default stream;
