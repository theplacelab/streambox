import { takeLatest, all, call } from "redux-saga/effects";
import * as actions from "state/actions";
import * as stream from "./stream.js";
import * as config from "./config.js";

export default function* rootSaga() {
  yield all([
    stream.periodicallyCheckStatus(),
    takeLatest(actions.GET_STREAM_STATUS, function* (action) {
      yield call(stream.getStatus, action.payload);
    }),
    takeLatest(actions.CONFIG_GET_TEMPLATE, config.getTemplate),
    takeLatest(actions.CONFIG_GET, config.getConfig),
    takeLatest(actions.CONFIG_UPDATE, config.updateConfig),
    takeLatest(actions.CONFIG_DELETE, config.deleteConfig),
    takeLatest(actions.CONFIG_START_STREAM, config.startStream),
    takeLatest(actions.CONFIG_STOP_STREAM, config.stopStream)
  ]);
}
