import { put, call, delay, select } from "redux-saga/effects";
import * as helper from "./sagaHelper.js";
import * as actions from "state/actions";
import { API } from "constants.js";

export const periodicallyCheckStatus = function* () {
  while (true) {
    //console.log("Checking status..");
    yield call(getStatus);
    yield delay(10000);
  }
};

export const getStatus = function* (action) {
  try {
    const response = yield helper.makeRequest(
      API.STREAMBOX_MGR,
      "GET",
      `status`
    );
    if (response.ok) {
      const payload = yield response.json();
      const reduxState = yield select();
      if (
        Object.keys(reduxState.stream.currentConfig).length === 0 &&
        payload.availableConfigs.length > 0
      ) {
        yield put({
          type: actions.CONFIG_GET,
          payload: payload.availableConfigs[0].id,
        });
      }
      if (reduxState.stream.configTemplate.length === 0) {
        yield put({
          type: actions.CONFIG_GET_TEMPLATE,
        });
      }
      yield put({
        type: actions.STREAM_STATUS_RESPONSE,
        payload,
      });
    } else {
      yield put({
        type: actions.STREAM_STATUS_RESPONSE,
        payload: {},
      });
    }
  } catch {
    yield put({
      type: actions.STREAM_STATUS_RESPONSE,
      payload: {},
    });
  }
};

export const getKeys = function* (action) {
  const response = yield helper.makeRequest(
    API.STREAMBOX_MGR,
    "GET",
    `setup/${action.payload.streamName}`,
    null,
    true
  );
  if (response.ok) {
    const res = yield response.json();
    yield put({
      type: actions.STREAM_KEYS_RESPONSE,
      payload: { ...res, name: action.payload.streamName },
    });
  } else {
    yield put({
      type: actions.STREAM_KEYS_RESPONSE,
      payload: {},
    });
  }
};
