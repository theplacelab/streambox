import * as actions from "state/actions";
import { put } from "redux-saga/effects";
import { API } from "constants.js";
import { makeRequest } from "./sagaHelper.js";

export const getTemplate = function* () {
  const response = yield makeRequest(
    API.STREAMBOX_MGR,
    "GET",
    `config/template`
  );
  const payload = yield response.json();
  if (response.ok) {
    yield put({
      type: actions.CONFIG_GET_TEMPLATE_RESPONSE,
      payload,
    });
  } else {
    console.warn(`Cannot get template`);
  }
};

export const getConfig = function* (action) {
  const id = action.payload;
  const response = yield makeRequest(API.STREAMBOX_MGR, "GET", `config/${id}`);
  const payload = yield response.json();
  if (response.ok) {
    yield put({
      type: actions.CONFIG_GET_RESPONSE,
      payload,
    });
  } else {
    console.warn(`Cannot get config ${id}`);
  }
};

export const updateConfig = function* (action) {
  const id = action.payload.id;
  if (id)
    yield put({
      type: actions.CONFIG_GET_RESPONSE,
      payload: action.payload,
    });
  const response = yield makeRequest(
    API.STREAMBOX_MGR,
    "POST",
    id ? `config/update/${id}` : "config/create",
    action.payload
  );
  const payload = yield response.json();
  if (!response.ok) {
    console.error(`Cannot create/update config ${id}`);
  } else {
    yield put({
      type: actions.CONFIG_GET_RESPONSE,
      payload,
    });
  }
  yield put({
    type: actions.GET_STREAM_STATUS,
  });
};

export const deleteConfig = function* (action) {
  const response = yield makeRequest(
    API.STREAMBOX_MGR,
    "GET",
    `config/delete/${action.payload.id}`
  );
  if (response.ok) {
    yield put({
      type: actions.GET_STREAM_STATUS,
    });
  } else {
    console.warn("Cannot delete config");
  }
};

export const startStream = function* (action) {
  const response = yield makeRequest(
    API.STREAMBOX_MGR,
    "GET",
    `stream/start/${action.payload.id}`
  );
  if (response.ok) {
    yield put({
      type: actions.GET_STREAM_STATUS,
    });
  } else {
    window.alert("Cannot start stream, check console");
    console.error("Cannot start stream");
  }
};

export const stopStream = function* (action) {
  const response = yield makeRequest(API.STREAMBOX_MGR, "GET", "stream/stop/");
  if (response.ok) {
    yield put({
      type: actions.GET_STREAM_STATUS,
    });
  } else {
    window.alert("Cannot start stream, check console");
    console.error("Cannot start stream");
  }
};
