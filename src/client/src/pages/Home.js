import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  configDelete,
  configGet,
  configStartStream,
  configUpdate,
  configStopStream
} from "state/actions";
import EditProfile from "components/EditProfile.js";

function Home() {
  const dispatch = useDispatch();
  const serverState = useSelector((redux) => redux.stream.serverStatus);
  const currentConfig = useSelector((redux) => redux.stream.currentConfig);
  const isStreaming = serverState.isStreaming;
  const [editMode, setEditMode] = useState(false);
  const [profile, setProfile] = useState({});
  const backendOnline = Object.keys(serverState).length > 0;
  const showInfo = false;

  const onEdit = (profile) => {
    setProfile(profile);
    setEditMode(true);
  };
  const toggleStream = () => {
    if (isStreaming) {
      dispatch(configStopStream());
    } else {
      dispatch(configStartStream(currentConfig));
    }
  };
  const onDelete = () => {
    if (window.confirm(`Delete "${currentConfig.name}?"`)) {
      dispatch(configDelete(currentConfig));
    }
  };
  const onChange = (e) => {
    dispatch(configGet(e.currentTarget.value));
  };

  return (
    <div style={styles.container}>
      {editMode && (
        <EditProfile
          profile={profile}
          onSave={(data) => {
            dispatch(configUpdate(data));
            setEditMode(false);
          }}
          onClose={() => setEditMode(false)}
        />
      )}
      <div style={styles.innerContainer}>
        <div style={{ display: "flex", flexDirection: "column" }}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              margin: "0 .5rem"
            }}
          >
            <div
              style={{
                width: "20rem",
                fontFamily: "Mulish",
                fontWeight: 900,
                margin: "auto"
              }}
            >
              {(serverState.availableConfigs?.length > 0 && (
                <select
                  onChange={onChange}
                  disabled={isStreaming}
                  style={styles.input}
                  value={currentConfig.id}
                >
                  {serverState.availableConfigs?.map((config) => {
                    return (
                      <option key={config.name} value={config.id}>
                        {config.name}
                      </option>
                    );
                  })}
                </select>
              )) || (
                <div
                  style={{
                    textAlign: "left",
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  {backendOnline && (
                    <React.Fragment>
                      <div style={styles.button}>
                        <FontAwesomeIcon icon="exclamation-triangle" />
                      </div>
                      <div> "Add at least one profile"</div>{" "}
                    </React.Fragment>
                  )}
                </div>
              )}
            </div>

            {backendOnline && (
              <div
                style={{
                  opacity: isStreaming ? 0.5 : 1,
                  width: "8rem",
                  display: "flex",
                  flexDirection: "row"
                }}
              >
                <div
                  style={styles.button}
                  onClick={isStreaming ? null : () => onEdit({})}
                >
                  <FontAwesomeIcon icon="plus-square" />
                </div>
                <div
                  style={styles.button}
                  onClick={isStreaming ? null : () => onEdit(currentConfig)}
                >
                  <FontAwesomeIcon icon="pen" />
                </div>
                <div
                  style={styles.button}
                  onClick={isStreaming ? null : onDelete}
                >
                  <FontAwesomeIcon icon="trash" />
                </div>
              </div>
            )}
          </div>

          <div style={{ ...styles.page, opacity: backendOnline ? 1 : 0.5 }}>
            <div onClick={toggleStream} style={styles.streamButton}>
              <div
                style={
                  isStreaming
                    ? {
                        ...styles.led,
                        ...styles.led_on
                      }
                    : {
                        ...styles.led,
                        ...styles.led_off
                      }
                }
              />
              <div style={styles.streamButtonText}>
                {backendOnline
                  ? isStreaming
                    ? "Stream: ON!"
                    : "Stream: OFF"
                  : ""}
              </div>
            </div>
          </div>
        </div>
        {backendOnline && (
          <div style={styles.infoBlock}>
            {Object.keys(currentConfig).map((key) => {
              if (!key.includes("streaming") && showInfo) {
                return (
                  <div style={styles.infoRow} key={key}>
                    <div style={styles.infoLabel}>{key}</div>
                    <div style={styles.infoData}>{currentConfig[key]}</div>
                  </div>
                );
              } else {
                return <div key={key}></div>;
              }
            })}
          </div>
        )}
      </div>
    </div>
  );
}

const styles = {
  container: {
    fontFamily: "Mulish",
    paddingTop: "5rem",
    fontSize: "1rem",
    width: "100vw"
  },
  innerContainer: {
    fontFamily: "Mulish",
    maxWidth: "30rem",
    margin: "auto"
  },
  page: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  streamButton: {
    margin: "1rem auto",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    background: "#cfcfcf",
    padding: "2rem",
    borderRadius: "0.3rem",
    width: "80%",
    filter: "drop-shadow(.1rem .1rem .2rem black)"
  },
  streamButtonText: {
    fontSize: "2rem",
    margin: "auto",
    fontWeight: "200",
    color: "black"
  },
  led: {
    minWidth: "5rem",
    height: "5rem",
    borderRadius: "10rem",
    border: "2px solid #333333"
  },
  led_on: {
    backgroundImage:
      "linear-gradient(-45deg, #000000, #2EB62C, #57C84D, #FF00FF, #83D475, #ABE098, #FFFFFF)",
    backgroundSize: "700% 700%",
    animation: "gradient 5s ease infinite",
    border: "2px solid #2EB62C"
  },
  led_off: {
    backgroundImage: "linear-gradient(-45deg, #eeeeee, #000000)",
    backgroundSize: "100% 100%",
    border: "2px solid rgb(117 117 117)"
  },
  input: {
    fontSize: "1.8rem",
    height: "3rem",
    width: "100%",
    borderRadius: ".5rem"
  },
  button: { margin: "0.5rem", fontSize: "2rem" },
  infoBlock: {
    display: "flex",
    flexDirection: "column",
    alignItems: "left",
    fontSize: "1rem",
    width: "100%",
    margin: "auto"
  },
  infoRow: {
    display: "flex",
    flexDirection: "row",
    border: 0,
    borderBottom: "1px solid #FFFFFF10"
  },
  infoLabel: { minWidth: "9rem", textAlign: "right" },
  infoData: {
    flexGrow: 1,
    textAlign: "left",
    marginLeft: "2rem"
  }
};
export default Home;
