import React from "react";
import { useSelector } from "react-redux";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Navigation() {
  const serverState = useSelector((redux) => redux.stream.serverStatus);
  const backendOnline = Object.keys(serverState).length > 0;
  const isStreaming = serverState.isStreaming;
  return (
    <div style={styles.container}>
      <div style={{ width: "10rem", flexGrow: "1" }}>Streambox</div>
      <div style={styles.infoBlock}>
        {(backendOnline && (
          <React.Fragment>
            <div style={styles.infoRow}>
              <div style={styles.infoLabel}>SERVER:</div>
              <div>{moment(serverState.serverStarted).fromNow()}</div>
            </div>
            <div style={styles.infoRow}>
              <div style={styles.infoLabel}>STREAM:</div>
              <div>
                {(isStreaming && moment(serverState.streamStarted).fromNow()) ||
                  "** Offline **"}
              </div>
            </div>
          </React.Fragment>
        )) || (
          <div style={styles.error}>
            <FontAwesomeIcon icon="exclamation-triangle" />
            &nbsp; NOT RUNNING!
          </div>
        )}
      </div>
    </div>
  );
}
export default Navigation;

const styles = {
  container: {
    fontFamily: "Mulish",
    textTransform: "uppercase",
    backgroundColor: "black",
    position: "fixed",
    width: "100%",
    textAlign: "left",
    padding: "1rem",
    display: "flex",
    flexDirection: "row",
    fontSize: "1.5rem",
    alignItems: "center"
  },
  infoLabel: { fontWeight: 200, marginRight: ".5rem" },
  infoBlock: {
    textAlign: "right",
    flexDirection: "column",
    width: "12rem",
    fontSize: "0.5rem",
    fontWeight: 900
  },
  infoRow: {
    display: "flex",
    flexDirection: "row",
    marginRight: "2rem"
  },
  error: {
    color: "orange",
    fontSize: ".9rem",
    display: "flex",
    flexDirection: "row",
    marginRight: "2rem"
  }
};
