import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import * as actions from "state/actions";

function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const user = useSelector((redux) => redux.user);
  const dispatch = useDispatch();

  const onLogin = (e) => {
    e.preventDefault();
    dispatch(actions.userLogin({ username, password }));
    setUsername("");
    setPassword("");
  };

  const onLogout = (e) => {
    dispatch(actions.userLogout());
  };

  if (user.isAuthenticated) {
    return (
      <div style={styles.containerAuth}>
        <div>{user.claims.name}</div>{" "}
        <div style={{ padding: "0 1rem 0 1rem" }} />
        <div style={styles.link} onClick={onLogout}>
          Logout
        </div>{" "}
        <div style={{ padding: "0 1rem 0 1rem" }} />
        {/*<div style={styles.link} onClick={onLogout}>
          Preferences
    </div>*/}
      </div>
    );
  } else {
    return (
      <form onSubmit={onLogin}>
        <div style={styles.container}>
          <div>
            <input
              autoComplete="current-username"
              value={username}
              style={styles.input}
              type="text"
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div>
            <input
              autoComplete="current-password"
              value={password}
              style={styles.input}
              type="password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div>
            <input style={styles.input} type="submit" value="Login" />
          </div>
        </div>
      </form>
    );
  }
}
export default Login;

const styles = {
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  containerAuth: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "grey",
    padding: "0 1rem 0 1rem",
  },
  input: {
    width: "10rem",
    fontSize: "2rem",
  },
  link: {
    color: "purple",
    textDecoration: "underline",
  },
};
