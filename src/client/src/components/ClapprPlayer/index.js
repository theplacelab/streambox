import React, { Component } from "react";
import PropTypes from "prop-types";
import Clappr from "clappr";

class ClapprPlayer extends Component {
  shouldComponentUpdate = () => {
    return false;
    //return nextProps.source !== this.props.source;
  };

  componentDidMount = () => {
    this.change(this.props.source);
  };

  componentWillUnmount = () => {
    if (this.player) this.destroyPlayer();
  };

  destroyPlayer = () => {
    if (this.player) this.player.destroy();
    this.player = null;
  };

  change = (source) => {
    if (this.player) this.destroyPlayer();
    fetch(source)
      .then((response) => {
        if (response.ok) {
          this.player = new Clappr.Player({
            parent: this.playerContainer,
            autoPlay: true,
            source,
            width: "100%",
            height: "100%",
            events: {
              onStop: () => {
                if (this.player) {
                  console.log("onStop!");
                  this.player.stop();
                }
              },
              onPause: () => {
                if (this.player) {
                  console.log("OnPause!");
                  this.player.pause();
                }
              },
              onError: () => {
                if (this.player) {
                  console.log("OnError!");
                  this.player.stop();
                }
              },
              onEnded: () => {
                if (this.player) {
                  console.log("OnEnded!");
                  this.player.play();
                }
              },
            },
          });
        } else {
          console.error(source);
        }
      })
      .catch(() => {
        console.error(source);
      });
  };

  render() {
    console.log(`Render ${this.player}`);
    return (
      <div>
        <div
          style={{ maxHeight: `35rem`, backgroundColor: "grey" }}
          id="clapprPlayer"
          ref={(el) => (this.playerContainer = el)}
        ></div>
      </div>
    );
  }
}
export default ClapprPlayer;

ClapprPlayer.defaultProps = {
  isBorderless: false,
};
ClapprPlayer.propTypes = {
  source: PropTypes.string,
};
