import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import Home from "pages/Home";
import { Provider } from "react-redux";
import store from "store";
import Navigation from "components/Navigation";
// import PrivateRoute from "components/user/PrivateRoute";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faPen,
  faPlusSquare,
  faTrash,
  faExclamationTriangle,
  faTimesCircle,
  faSave,
} from "@fortawesome/free-solid-svg-icons";
library.add(
  faPlusSquare,
  faPen,
  faTrash,
  faExclamationTriangle,
  faTimesCircle,
  faSave
);

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Navigation></Navigation>
          <Switch>
            <Route path="/" component={Home} />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
