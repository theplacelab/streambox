const { sendResponse } = require("./util");
const ffmpeg = require("fluent-ffmpeg");
const fs = require("fs");
const filePath = "./config";
module.exports = {
  restart: ({ req, res, state }) => {
    if (state.task) {
      state.task.kill("SIGINT");
      state.set({ task: null, lastEvent: Date.now(), err: "" });
    }
    module.exports.start({ req, res, state, skipCheck: true });
  },
  start: ({ req, res, state, bot }) => {
    const configFile = `${filePath}/${req.params.id}.json`;
    fs.readFile(configFile, "utf8", (err, data) => {
      if (err) {
        console.error(err);
        sendResponse(res, 400, err.message);
      } else {
        if (state.task) {
          sendResponse(
            res,
            409,
            "Stream already running, stop first (or send restart)"
          );
          return;
        } else {
          data = JSON.parse(data);
          const streamServer = `${data.streaming_server}/${data.streaming_key}`;
          let task = ffmpeg(data.source, {
            thread_queue_size: data.thread_queue_size || 1024,
            movflags: "+faststart",
            niceness: data.niceness || -10
          })
            .inputOptions(["-f alsa"])
            .audioBitrate(data.bitrate || 128)
            .audioFrequency(data.frequency || 22050)
            .audioCodec(data.audio_codec || "aac")
            .videoCodec(data.video_codec || "libx264")
            .audioFilter(`volume=${data.gain || 1}`)
            .format("flv")
            .output(streamServer)
            .on("error", (err) => {
              state.set({
                task: null,
                lastEvent: Date.now(),
                err: err.message
              });
              if (!err.message.includes("normally"))
                if (process.env.TELEGRAM_BOT_ENABLE)
                  bot?.announce("💀 Stream Crash!");
              try {
                sendResponse(res, 500, err.message);
              } catch {}
              console.error(err);
            })
            .on("start", (command) => {
              if (process.env.TELEGRAM_BOT_ENABLE)
                bot?.announce(
                  `🟢 ${data.name} (${data.bitrate}${data.audio_codec})\n ${data.public_link}`
                );
              sendResponse(res, 200, "Stream starting");
              state.set({
                streamStarted: Date.now()
              });
            });
          state.set({ task, lastEvent: Date.now(), err: "" });
          task.run();
        }
      }
    });
  },
  stop: ({ res, state, bot }) => {
    if (state.task) {
      if (process.env.TELEGRAM_BOT_ENABLE) bot?.announce("🛑 Stream Stopped");
      state.task.kill("SIGINT");
      state.set({ task: null, lastEvent: Date.now(), err: "" });
      sendResponse(res, 200, "Stream being stopped");
    } else {
      sendResponse(res, 409, "Stream not running");
    }
  }
};
