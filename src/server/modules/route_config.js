const { sendResponse } = require("./util");
const { v4: uuidv4 } = require("uuid");
const fs = require("fs");
const configDir = `${__dirname}/../config`;

const configTemplateFile = require(`${configDir}/template.json`);
let configTemplate = {};
configTemplateFile.map((item) => {
  configTemplate[item.name] = item.default;
});

module.exports = {
  getTemplate: ({ res }) => {
    sendResponse(res, 200, configTemplateFile);
  },
  writeConfig: ({ req, res }) => {
    const recordConfig = (newConfig) => {
      const configFile = `${configDir}/${newConfig.id}.json`;
      fs.writeFile(
        configFile,
        JSON.stringify(newConfig),
        {
          encoding: "utf8",
          flag: "w"
        },
        (err) => {
          if (err) {
            console.error(err);
            sendResponse(res, 500, err.message);
          } else {
            sendResponse(res, 200, newConfig);
          }
        }
      );
    };
    if (!req.params.id) {
      recordConfig({
        ...configTemplate,
        ...req.body,
        id: uuidv4()
      });
    } else {
      const configFile = `${configDir}/${req.params.id}.json`;
      fs.readFile(configFile, "utf8", (err, data) => {
        if (err) {
          console.error(err);
          sendResponse(res, 400, err.message);
        } else {
          recordConfig({
            ...configTemplate,
            ...JSON.parse(data),
            ...req.body
          });
        }
      });
    }
  },
  readConfig: ({ req, res }) => {
    const configFile = `${configDir}/${req.params.id}.json`;
    fs.readFile(configFile, "utf8", (err, data) => {
      if (err) {
        console.error(err);
        sendResponse(res, 400, err.message);
      } else {
        sendResponse(res, 200, JSON.parse(data));
      }
    });
  },
  deleteConfig: ({ req, res }) => {
    const configFile = `${configDir}/${req.params.id}.json`;
    fs.unlink(configFile, (err) => {
      if (err) {
        console.error(err);
        sendResponse(res, 400, err.message);
      } else {
        sendResponse(res, 200, "Config deleted");
      }
    });
  }
};
