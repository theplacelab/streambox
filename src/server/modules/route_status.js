const fs = require("fs");
const { sendResponse } = require("./util");
const configDir = `${__dirname}/../config`;
module.exports = {
  info: ({ res, state }) => {
    fs.readdir(`${configDir}`, (err, filenames) => {
      if (err) {
        console.error(err);
        return;
      }
      const availableConfigs = [];
      let counter = 0;
      filenames.forEach((filename) => {
        fs.readFile(`${configDir}/${filename}`, "utf-8", (err, data) => {
          if (err) {
            console.error(err);
          } else {
            const json = JSON.parse(data);
            if (filename !== "template.json")
              availableConfigs.push({ name: json.name, id: json.id });
          }
          counter++;
          if (filenames.length === counter) {
            sendResponse(res, 200, {
              availableConfigs: availableConfigs.sort((a, b) => {
                return a.name > b.name;
              }),
              isStreaming: state.task ? true : false,
              serverStarted: state.serverStarted,
              lastEvent: state.lastEvent,
              err: state.err
            });
          }
        });
      });
    });
  }
};
