"use strict";
require("dotenv").config({ path: require("find-config")(".env") });
const middleware = require("./modules/middleware.js");
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();
const fs = require("fs");
const https = require("https");
const util = require("./modules/util.js");
const route_status = require("./modules/route_status.js");
const route_stream = require("./modules/route_stream.js");
const route_config = require("./modules/route_config.js");
const { Telegraf } = require("telegraf");
const path = require("node:path");
let state = {
  serverStarted: Date.now(),
  lastEvent: null
};
state.set = (val) => {
  state = { ...state, ...val };
};

const port = process.env.PORT || 8888;
console.log(`\n--------------------------------------`);
console.log(`Streambox Support Server`);
console.log(`Port: ${port}`);
console.log(`TelegramBot: ${process.env.TELEGRAM_BOT_LINK || NONE}`);
console.log(`--------------------------------------`);

let bot = null;
if (
  process.env.TELEGRAM_BOT_ENABLE === true &&
  process.env.TELEGRAM_BOT_TOKEN &&
  process.env.TELEGRAM_CHANNEL_ID
) {
  bot = new Telegraf(process.env.TELEGRAM_BOT_TOKEN);
  bot.hears("status", (ctx) =>
    ctx.reply(state.task ? "Streaming!" : "Currently offline")
  );
  bot.launch();
  bot.announce = (msg) =>
    bot.telegram.sendMessage(process.env.TELEGRAM_CHANNEL_ID, msg);
  process.once("SIGINT", () => bot.stop("SIGINT"));
  process.once("SIGTERM", () => bot.stop("SIGTERM"));
}

app.use(cors());
app.use(express.json());
app.engine("html", require("ejs").renderFile);
app.listen(port);

app.get("/status", (req, res) => {
  middleware.verifyWithFallback(
    req,
    res,
    () => route_status.infoSecure({ req, res, state }),
    () => route_status.info({ req, res, state })
  );
});

app.use(express.static("www"));

// STREAM routes
app.get("/stream/start/:id", (req, res) => {
  route_stream.start({ req, res, state, bot });
});

app.post("/stream/launch", (req, res) => {
  route_stream.launch({ req, res, state });
});

app.get("/stream/restart", (req, res) => {
  route_stream.restart({ req, res, state });
});

app.get("/stream/stop", (req, res) => {
  route_stream.stop({ req, res, state, bot });
});

// CONFIG routes
app.get("/config/template", (req, res) => {
  route_config.getTemplate({ req, res, state });
});

app.get("/config/:id", (req, res) => {
  route_config.readConfig({ req, res, state });
});

app.post("/config/update/:id", (req, res) => {
  route_config.writeConfig({ req, res, state });
});

app.post("/config/create", (req, res) => {
  route_config.writeConfig({ req, res, state });
});

app.get("/config/delete/:id", (req, res) => {
  route_config.deleteConfig({ req, res, state });
});

// Telegram
app.get("/telegram/test", (req, res) => {
  route_telegram.test({ req, res, state, bot });
});

// DEFAULT route
app.use(function (req, res, setCommand) {
  util.log(`REJECTING: ${req.method}:${req.originalUrl}`);
  res.sendStatus(404);
});
