# Streambox

Web app wrapper to ffmpeg, allows one-button streaming from linux box to the world. Can be integrated with [https://gitlab.com/theplacelab/image/librespot]

☕ &nbsp; _Is this project helpful to you? [Please consider buying me a coffee](https://pay.feralresearch.org/tip)._

## Docker

There is a docker image, it requires privileged access to get to the USB record player. Not a problem in a private setting but you might want to figure out something more nuanced on a shared server:

```
docker run -d \
--restart=unless-stopped \
-p 8888:8888 \
--privileged \
-v /dev/bus/usb:/dev/bus/usb \
-v *PATH TO CONFIG*:/usr/src/app/config \
-v *PATH TO .env*:/usr/src/app/.env \
registry.gitlab.com/feralresearch/streambox:latest
```

## License

- MIT Licensed
- This project makes use of FontAwesome icons under the [Creative Commons Attribution 4.0 International license](https://fontawesome.com/license)
